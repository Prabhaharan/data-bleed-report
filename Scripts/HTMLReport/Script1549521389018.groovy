import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.relevantcodes.extentreports.ExtentReports as ExtentReports
import com.relevantcodes.extentreports.ExtentTest as ExtentTest
import com.relevantcodes.extentreports.LogStatus as LogStatus
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.DateFormat;
import java.util.Calendar;
import org.apache.commons.io.FileUtils
import org.apache.commons.io.FilenameUtils

//variable declaration
def datecontentfile = ""
GlobalVariable.Environment = ""
def envr =""
String pattern = "MM-dd-yyyy";
SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
String dateTodayFolder = simpleDateFormat.format(new Date());
List<String> jsonfile  = new ArrayList<String>()
def datableedScenariodetails = datableedScenariosheet.getAllData()
def scenarionameData=""
def scenarioheadingData=""
def scenariogoalData=""
def wikilinkData =""
int ROW= 1
List<String> scenarioName = new ArrayList<String>()
Map<String, Map<String,String>> scenario = new LinkedHashMap<String, Map<String,String>>()
Map<String, String> description = new LinkedHashMap<String,String>()
for(def datableedScenariodata :datableedScenariodetails){
	scenarionameData = datableedScenariosheet.getValue('Scenario No.',ROW)
	scenarioheadingData = datableedScenariosheet.getValue('Scenario Summary Heading',ROW)
	scenariogoalData = datableedScenariosheet.getValue('Scenario Goal',ROW)
	wikilinkData = datableedScenariosheet.getValue('Wiki Docuemntation',ROW)
	
	scenarioName.add(scenarionameData)
	if(scenario.get(scenarionameData) == null) {
		description = new LinkedHashMap<String,String>()
	} else {
		description = scenario.get(scenarionameData)
	}
	description.put(scenarioheadingData, scenariogoalData)
	scenario.put(scenarionameData,description)
	ROW++
}

println(scenarioName)

BufferedReader brenv = new BufferedReader(new FileReader(GlobalVariable.envlocation));
try
{
	StringBuilder sb = new StringBuilder();
	String line = brenv.readLine();
	while (line != null)
	{
		sb.append(line);
		sb.append("\n");
		line = brenv.readLine();
	}
	 
	 envr =sb.toString()
	 GlobalVariable.Environment=envr.trim()
	println GlobalVariable.Environment
 }
finally
{
		brenv.close();
}

def filerelativelocation=GlobalVariable.datableedfilelocation
println("file path"+filerelativelocation)
File datefolder = new File(filerelativelocation);
File[] listOfdateFiles = datefolder.listFiles();
for (File file : listOfdateFiles)
{
	def filecontent =filerelativelocation+file.getName()
	
	File contentfolder = new File(filecontent+"\\"+GlobalVariable.Environment);
	File[] listOfContentFiles = contentfolder.listFiles();
	for (File contentfile : listOfContentFiles)
	{
		println("file content"+contentfile)
	  if(contentfile.getName().equals(dateTodayFolder)){
		  println("file name"+contentfile.getName())
		  println("date name"+dateTodayFolder)
		  datecontentfile = file.getName()
	  }
	}
	
	
}

//D:\Archive\DataBleedReport\DataBleedDev03\02-12-2019
	def filelocation=filerelativelocation+datecontentfile+"\\"+GlobalVariable.Environment+"\\"+dateTodayFolder
	println("date name"+filelocation)
	File folder = new File(filelocation);
	File[] listOfFiles = folder.listFiles();
	
	
	for (File file : listOfFiles)
	{
		if( FileUtils.sizeOfDirectory(file) > 0 && file.exists()){
		jsonfile.add(file.getName())
		}
		
	}
		
	
			
			
			for (Map.Entry<String, Map<String, List<String>>> scenariojsonMap : scenario.entrySet())
			{
				ExtentReports repScenario = CustomKeywords.'reports.ReportFile.getInstance'(reportFolder)
				ExtentTest testScenario
				println testScenario
				//def TestCase=scenariojsonMap.getKey()
				def foldeNam
				println(testScenario)
				testScenario =repScenario.startTest("<a href=#>"+scenariojsonMap.getKey()+"</a>")
				foldeNam=scenariojsonMap.getKey()
					println("Mapping"+scenariojsonMap.getKey())
					for (Map.Entry<String, String> descriptionMap : scenariojsonMap.getValue().entrySet())
					{
						println("Key= " + descriptionMap.getKey() + ", Value = " + descriptionMap.getValue());
						testScenario.setDescription("<b><h6> ENV : env </b></h6> ".replace("env",GlobalVariable.Environment ) +
							"  <b><h6> Date :date </b></h6>".replace("date", dateTodayFolder) +
							"  <b><h6>"+descriptionMap.getKey()+":"+"</b></h6>"+
							"  <b><h6>"+descriptionMap.getValue()+"</b></h6>" +
							"  <b><h6>"+"For details, please refer the "+"<a href="+wikilinkData+">wiki page</a>"+"</b></h6>")
					}
					if(jsonfile.contains(scenariojsonMap.getKey()))
					{
					def jsonfolderlocation = scenariojsonMap.getKey()
					File locationfolder = new File(filelocation+"/"+jsonfolderlocation);
					File[] listOfLocationFiles = locationfolder.listFiles()
					for (File locationfile : listOfLocationFiles)
					{
						def jsonfilelocation = locationfile.getName()
						BufferedReader br = new BufferedReader(new FileReader(filelocation+"/"+jsonfolderlocation+"/"+jsonfilelocation));
						try
						{
							StringBuilder sb = new StringBuilder();
							String line = br.readLine();
							while (line != null)
							{
								sb.append(line);
								sb.append("\n");
								line = br.readLine();
							}
							String read =sb.toString()
							def reason= read.replace("{","").replace("}","").split(":")
							println reason[1]
							testScenario.log(LogStatus.FAIL, reason[1])

						 }
						finally
						{
								br.close();
						}
						
					}
					testScenario.log(LogStatus.FAIL, foldeNam);
					repScenario.endTest(testScenario);
					repScenario.flush();
					
				}else{
				
				testScenario.log(LogStatus.PASS,"NO DATA_BLEED OCCURED")
				testScenario.log(LogStatus.PASS, foldeNam);
				repScenario.endTest(testScenario);
				repScenario.flush();
				
				}
			}		
	
	/*List emptyFileLocatio1n = new ArrayList(scenarioName);
	emptyFileLocation.removeAll(jsonfile);
	println emptyFileLocation
	for(def emptyfile :emptyFileLocation){
		ExtentReports repScenario = CustomKeywords.'reports.ReportFile.getInstance'(reportFolder)
		ExtentTest testScenario
		println testScenario
		def TestCase=emptyfile
		testScenario =repScenario.startTest(TestCase)
		println(testScenario)
		for (Map.Entry<String, Map<String, List<String>>> scenarioMap : scenario.entrySet())
		{
			if(scenarioMap.getKey().contentEquals(emptyfile))
			{
				for (Map.Entry<String, String> descriptionMapEmpty : scenarioMap.getValue().entrySet())
				{
					
					println("Key= " + descriptionMapEmpty.getKey() + ", Value = " + descriptionMapEmpty.getValue());
						testScenario.setDescription("<b><h6> ENV : env </b></h6> ".replace("env",GlobalVariable.Environment ) +
							"  <b><h6> Date :date </b></h6>".replace("date", dateTodayFolder) +
							"  <b><h6>"+descriptionMapEmpty.getKey()+":"+"</b></h6>"+
							" <b><h6>"+descriptionMapEmpty.getValue()+"</b></h6>" +
							"  <b><h6>"+"For details, please refer the "+"<a href="+wikilinkData+">wiki page</a>"+"</b></h6>" )
					
				}
				testScenario.log(LogStatus.PASS,"NO DATA_BLEED OCCURED")
				testScenario.log(LogStatus.PASS, TestCase);
				repScenario.endTest(testScenario);
				repScenario.flush();
			}
		}		
			
	}
	*/
