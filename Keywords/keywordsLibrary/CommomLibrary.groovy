
package keywordsLibrary

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import org.openqa.selenium.TakesScreenshot
import org.openqa.selenium.WebDriver
import keywordsLibrary.CommomLibrary
import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.interactions.Actions as Actions
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.openqa.selenium.WebElement as WebElement

import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.interactions.Actions as Actions
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.JavascriptExecutor as JavascriptExecutor


public class CommomLibrary {
	@Keyword
	def clickUsingJS(TestObject to, int timeout){
		WebDriver driver = DriverFactory.getWebDriver()
		WebElement element = WebUiCommonHelper.findWebElement(to, timeout)
		JavascriptExecutor executor = ((driver) as JavascriptExecutor)
		executor.executeScript('arguments[0].click()', element)
	}
	@Keyword
	public String randomText(){
		Random randomGenerator = new Random();
		String randomChar = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		StringBuffer randStr = new StringBuffer();
		for(int i=0; i<5; i++){
			int randomNumber =randomGenerator.nextInt(randomChar.length());
			char ch = randomChar.charAt(randomNumber);
			randStr.append(ch);
		}
		return randStr;
	}

	@Keyword
	public void addGlobalVariable(String name, def value) {
		GroovyShell shell1 = new GroovyShell()
		MetaClass mc = shell1.evaluate("internal.GlobalVariable").metaClass
		String getterName = "get" + name.capitalize()
		mc.'static'."$getterName" = { -> return value }
		mc.'static'."$name" = value
	}

	@Keyword
	public static String getRandomNumber() {
		Random rand = new Random();
		int n = rand.nextInt(9999) + 1;
		String RandomNumber=n;
		return RandomNumber;
	}
	@Keyword
	public static String getRandomNumberuptofivedigit() {
		Random rand = new Random();
		int n = (10000 + rand.nextInt(90000))
		String RandomNumber=n;
		return RandomNumber;
	}
	@Keyword
	public static String getRandomNumberuptotendigit() {
		Random rand = new Random();
		int n = (1000000000 + rand.nextInt(900000000))
		String RandomNumber=n;
		return RandomNumber;
	}
	@Keyword
	public TestObject dynamicElement(String xpath){
		TestObject patientSearchObject = findTestObject('Base/commanXpath')
		patientSearchObject.findProperty('xpath').setValue(xpath)
		return patientSearchObject
	}
	@Keyword
	public TestObject dynamicElements(String xpath){
		TestObject patientSearchObject = findTestObject('Base/CommonPath')
		patientSearchObject.findProperty('xpath').setValue(xpath)
		return patientSearchObject
	}

	//Random Single Value Text Generator
	@Keyword
	public String randomSingleText(){
		Random randomGenerator = new Random();
		String randomSingleChar = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		StringBuffer randSingleStr = new StringBuffer();
		for(int i=0; i<1; i++){
			char ch = randomSingleChar.charAt(randomGenerator.nextInt(randomSingleChar.length()));
			randSingleStr.append(ch);
		}
		return randSingleStr;
	}

	//Multiple time click when not able to click the element
	@Keyword
	public String carePlanElementClick(TestObject To){
		String clickStatus
		for(int count=0;count<=2;count++){
			try {
				WebUI.click(To, FailureHandling.STOP_ON_FAILURE)
				clickStatus="Pass"
				break
			} catch (Exception e) {
				clickStatus=e.toString()
			}
		}
		return clickStatus
	}
	@Keyword
	public String clickMouseOver(TestObject to){
		String clickStatus
		try{
			WebDriver driver = DriverFactory.getWebDriver()
			WebElement element = WebUiCommonHelper.findWebElement(to,3)
			Actions action = new Actions(driver);
			action.moveToElement(element).click().build().perform();
			clickStatus="Pass"
		}catch(Exception e){
			clickStatus=e.toString()
		}
		return clickStatus
	}

	//Random  Value Text Generator
	@Keyword
	public String randomTextalone(){
		Random randomGenerator = new Random();
		String randomtextChar = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		StringBuffer randtextStr = new StringBuffer();
		for(int i=0; i<6; i++){
			char ch = randomtextChar.charAt(randomGenerator.nextInt(randomtextChar.length()));
			randtextStr.append(ch);
		}
		return randtextStr;
	}
	@Keyword
	public String randomTextaloneEmail(){
		Random randomGenerator = new Random();
		String randomtextChar = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		StringBuffer randtextStr = new StringBuffer();
		for(int i=0; i<6; i++){
			char ch = randomtextChar.charAt(randomGenerator.nextInt(randomtextChar.length()));
			randtextStr.append(ch);
		}
		return randtextStr + "@gsihealth.com";
	}
}
