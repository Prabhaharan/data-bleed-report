package reports

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import junit.framework.TestListener
import sun.security.ssl.Alerts
import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable

import com.relevantcodes.extentreports.DisplayOrder;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.Alert
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot
import org.openqa.selenium.WebDriver

import org.apache.commons.io.FileUtils
import java.awt.Rectangle
import java.awt.Robot
import java.awt.image.BufferedImage
import java.io.IOException
import java.text.SimpleDateFormat
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import org.jsoup.nodes.Document
import org.jsoup.select.Elements
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.interactions.Actions as Actions
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import java.awt.Toolkit
import javax.imageio.ImageIO;
import keywordsLibrary.CommomLibrary



public class ReportFile {
	private static ExtentReports extent;
	private static ExtentReports extentScenario;
	public static String folderPath
	@Keyword
	public String propertyFileForReport(String reportFolder){
		try{
			File dir = new File(".\\configuration\\"+reportFolder)
			if(!dir.isDirectory()){
				dir.mkdir()
			}
			File configFile= new File(".\\configuration\\"+reportFolder+"\\reportConfig.txt")
			if(configFile.exists() && configFile.isDirectory()) {
				configFile.delete()
			}
			WebUI.delay(2)
			configFile.createNewFile()
			Date date=new Date();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd hh:mm:ss aa z");
			String fileName=dateFormat.format(date).toString()
			println fileName
			BufferedWriter writer = new BufferedWriter(new FileWriter(".\\configuration\\"+reportFolder+"\\reportConfig.txt"));
			writer.write(fileName);
			writer.close()
			WebUI.delay(1)
			return "Success"
		}catch(Exception e){
			return "Fail"+e
		}
	}
	@Keyword
	public static ExtentReports getInstance(String reportFolder){
		int extendCount=0
		GlobalVariable.folderPath
		if(GlobalVariable.folderPath.equals("")){
		
			String pattern = "MM-dd-yyyy";
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
			GlobalVariable.folderPath = simpleDateFormat.format(new Date());
		}
		if (extent == null) {
			
			String fileName=GlobalVariable.folderPath +".html";
			extent = new ExtentReports(System.getProperty("user.dir")+"\\CustomizedReports\\"+reportFolder+"\\"+fileName, true, DisplayOrder.NEWEST_FIRST);
			extent.addSystemInfo("Selenium Version", "2.53.0").addSystemInfo(
					"Environment", "QA");
		}

		return extent;
	}
	@Keyword
	public static ExtentReports getInstanceScenario(String reportFolder){
		int extendCount=0
		
		if (extentScenario == null) {
			//Date d=new Date();
			String fileName=GlobalVariable.Environment+".html";

			extentScenario = new ExtentReports(System.getProperty("user.dir")+"\\CustomizedReports\\"+fileName, true, DisplayOrder.NEWEST_FIRST);
			extentScenario.addSystemInfo("Selenium Version", "2.53.0").addSystemInfo(
					"Environment", "QA");
		}

		return extentScenario;
	}
	@Keyword
	public String print(ExtentTest test,boolean verifyStatus,String printStatement) {
		if(verifyStatus){
			test.log(LogStatus.INFO, printStatement);
			return "Pass"
		}else{
			test.log(LogStatus.FAIL, "Fail : "+printStatement);
			takeScreenshot(test)
			return "Fail"
		}
	}
	@Keyword
	public String printTS(ExtentTest test,Map<String,Boolean> statusStatement ) {
		String returnStatus= "Fail"
		String printStatement=""
		List<String> status= new ArrayList<String>()
		try {
			Iterator resultEntry=statusStatement.entrySet().iterator()
			while(resultEntry.hasNext()){
				Map.Entry<String,Boolean> resultPair=(Map.Entry<String,Boolean>)resultEntry.next()
				if(resultPair.getValue()){
					printStatement= printStatement+ "&nbsp;&nbsp;&nbsp; Pass : "+resultPair.getKey()+"\n <br>"
					status.add("Pass")
				}else{
					printStatement= printStatement+"&nbsp;&nbsp;&nbsp; Fail : "+resultPair.getKey()+"\n <br>"
					status.add("Fail")
				}
			}
			if(!status.contains("Fail")){
				test.log(LogStatus.INFO, printStatement);
				returnStatus= "Pass"
			}else{
				test.log(LogStatus.FAIL, printStatement);
			}
		} catch (Exception e) {
			test.log(LogStatus.INFO, " After Statement Print Exception "+e);
		}

		return returnStatus
	}
	@Keyword
	public String takeScreenshot(ExtentTest test) {
		WebDriver driver = DriverFactory.getWebDriver()
		Date d = new Date();
		String screenshotFile = d.toString().replace(":", "_").replace(" ", "_")+"-" +CommomLibrary.getRandomNumber()+ ".png";
		try{
			WebUI.takeScreenshot(System.getProperty("user.dir")+"\\screenshots\\" + screenshotFile)
			test.log(LogStatus.INFO, "Screenshot-> "
					+ test.addScreenCapture(System.getProperty("user.dir")+"\\screenshots\\"+ screenshotFile));
		}catch(Exception e){
			test.log(LogStatus.INFO, "**Unexpected Alert Pop is opened - Cannot take screenshot ");
		}
	}
}
