
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import com.kms.katalon.core.testobject.TestObject

import java.lang.String

import com.relevantcodes.extentreports.ExtentTest

import java.util.Map


def static "keywordsLibrary.CommomLibrary.clickUsingJS"(
    	TestObject to	
     , 	int timeout	) {
    (new keywordsLibrary.CommomLibrary()).clickUsingJS(
        	to
         , 	timeout)
}

def static "keywordsLibrary.CommomLibrary.randomText"() {
    (new keywordsLibrary.CommomLibrary()).randomText()
}

def static "keywordsLibrary.CommomLibrary.addGlobalVariable"(
    	String name	
     , 	Object value	) {
    (new keywordsLibrary.CommomLibrary()).addGlobalVariable(
        	name
         , 	value)
}

def static "keywordsLibrary.CommomLibrary.getRandomNumber"() {
    (new keywordsLibrary.CommomLibrary()).getRandomNumber()
}

def static "keywordsLibrary.CommomLibrary.getRandomNumberuptofivedigit"() {
    (new keywordsLibrary.CommomLibrary()).getRandomNumberuptofivedigit()
}

def static "keywordsLibrary.CommomLibrary.getRandomNumberuptotendigit"() {
    (new keywordsLibrary.CommomLibrary()).getRandomNumberuptotendigit()
}

def static "keywordsLibrary.CommomLibrary.dynamicElement"(
    	String xpath	) {
    (new keywordsLibrary.CommomLibrary()).dynamicElement(
        	xpath)
}

def static "keywordsLibrary.CommomLibrary.dynamicElements"(
    	String xpath	) {
    (new keywordsLibrary.CommomLibrary()).dynamicElements(
        	xpath)
}

def static "keywordsLibrary.CommomLibrary.randomSingleText"() {
    (new keywordsLibrary.CommomLibrary()).randomSingleText()
}

def static "keywordsLibrary.CommomLibrary.carePlanElementClick"(
    	TestObject To	) {
    (new keywordsLibrary.CommomLibrary()).carePlanElementClick(
        	To)
}

def static "keywordsLibrary.CommomLibrary.clickMouseOver"(
    	TestObject to	) {
    (new keywordsLibrary.CommomLibrary()).clickMouseOver(
        	to)
}

def static "keywordsLibrary.CommomLibrary.randomTextalone"() {
    (new keywordsLibrary.CommomLibrary()).randomTextalone()
}

def static "keywordsLibrary.CommomLibrary.randomTextaloneEmail"() {
    (new keywordsLibrary.CommomLibrary()).randomTextaloneEmail()
}

def static "reports.ReportFile.propertyFileForReport"(
    	String reportFolder	) {
    (new reports.ReportFile()).propertyFileForReport(
        	reportFolder)
}

def static "reports.ReportFile.getInstance"(
    	String reportFolder	) {
    (new reports.ReportFile()).getInstance(
        	reportFolder)
}

def static "reports.ReportFile.getInstanceScenario"(
    	String reportFolder	) {
    (new reports.ReportFile()).getInstanceScenario(
        	reportFolder)
}

def static "reports.ReportFile.print"(
    	ExtentTest test	
     , 	boolean verifyStatus	
     , 	String printStatement	) {
    (new reports.ReportFile()).print(
        	test
         , 	verifyStatus
         , 	printStatement)
}

def static "reports.ReportFile.printTS"(
    	ExtentTest test	
     , 	java.util.Map<String, java.lang.Boolean> statusStatement	) {
    (new reports.ReportFile()).printTS(
        	test
         , 	statusStatement)
}

def static "reports.ReportFile.takeScreenshot"(
    	ExtentTest test	) {
    (new reports.ReportFile()).takeScreenshot(
        	test)
}
