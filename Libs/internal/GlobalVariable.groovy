package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object datableedfilelocation
     
    /**
     * <p></p>
     */
    public static Object folderPath
     
    /**
     * <p></p>
     */
    public static Object envlocation
     
    /**
     * <p></p>
     */
    public static Object Environment
     

    static {
        def allVariables = [:]        
        allVariables.put('default', ['datableedfilelocation' : 'D:\\Archive\\DataBleedReport\\', 'folderPath' : '', 'envlocation' : 'D:\\Archive\\out.txt', 'Environment' : ''])
        
        String profileName = RunConfiguration.getExecutionProfile()
        
        def selectedVariables = allVariables[profileName]
        datableedfilelocation = selectedVariables['datableedfilelocation']
        folderPath = selectedVariables['folderPath']
        envlocation = selectedVariables['envlocation']
        Environment = selectedVariables['Environment']
        
    }
}
