<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>DataBleedReport</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-14T07:48:55</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>0057f033-80c9-4673-8a3e-1b64bbe5258b</testSuiteGuid>
   <testCaseLink>
      <guid>103947e8-bdf6-457f-8fd5-98fcf2685aac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/HTMLReport</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>75339eac-e195-485b-8e16-6b8b7273eeb6</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ee985936-b8ff-4f2c-adef-e5abb5c1cea4</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>SCRIPT_VARIABLE</type>
         <value>findTestData(&quot;DataBleed/Scenario Details&quot;)</value>
         <variableId>6fa9a164-4014-4c2b-bcc4-aa254742991c</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
